<?php

class Extra
{
	public $x;
	public $y;
	public $z;

	public function __construct($x = 1, $y = 2, $z = 3)
	{
		$this->x = $x;
		$this->y = $y;
		$this->z = $z;
	}

	public function __toString()
	{
		return "This message will be printed if i try to echo object";
	}

	public function __get($property)
	{
		echo 'Property does not exist';
	}

	public function __set($name, $value)
	{
		$this->{$name} = $value;
	}

	public function showData()
	{
		echo $this->x . ' ' . $this->y . ' ' . $this->z;
	}
}