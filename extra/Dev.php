<?php

require 'extra/Foo.php';
require 'extra/Bar.php';

class Dev implements Foo, Bar
{
	public static $dev = 'dev static value';

	public static function development()
	{
		echo 'Static function';
	}

	public function foo()
	{
		return 'foo';
	}

	public function bar()
	{
		return 'bar';
	}

	public static function printResults()
	{
		echo self::foo() . self::bar();
	}
}