<?php

class User 
{
	public $name;
	public $email;
	private $age;
	protected $height;

	public function __construct($name = 'test', $email = 'test@gmail.com', $age = 1, $height = 180)
	{
		$this->name = $name;
		$this->email = $email;
		$this->age = $age;
		$this->height = $height;
	}

	public function __destruct()
	{
		echo 'I am run at the end of php script!';
		echo "<br>";
	}

	public function userName()
	{
		echo 'User name:' . $this->name;
		echo "<br>";
	}

	public function userEmail()
	{
		echo 'User email:' . $this->email;
		echo "<br>";
	}

	public function userAge()
	{
		echo 'User age:' . $this->age;
		echo "<br>";
	}

	public function userHeight()
	{
		echo 'User height:' . $this->height;
		echo "<br>";
	}

	public function userDetails()
	{
		echo 'User details: ' . $this->name . $this->email . $this->age . $this->height;
		echo "<br>";
	}
}