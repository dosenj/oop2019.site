<?php

class Test
{
	public const PUBLIC_VARIABLE = 'public';

	protected const PROTECTED_VARIABLE = 'protected';

	private const PRIVATE_VARIABLE = 'private';

	public function __construct()
	{
		echo 'Test function constructor';
		echo "<br>";
	}

	public function publicTest()
	{
		echo 'Public function';
		echo "<br>";
	}

	protected function protectedTest()
	{
		echo 'Protected function';
		echo "<br>";
	}

	private function privateTest()
	{
		echo 'Private function';
		echo "<br>";
	}

	public function runAllFunctions()
	{
		$this->publicTest();
		$this->protectedTest();
		$this->privateTest();
	}

	public function showConstants()
	{
		echo self::PUBLIC_VARIABLE . ' ' . self::PROTECTED_VARIABLE . ' ' . self::PRIVATE_VARIABLE;
		echo "<br>";
	}
}