<?php

class Testing extends Test
{
	public function __construct()
	{
		parent::__construct(); // call parent constructor
		echo 'Testing constructor data';
		echo "<br>";
	}

	protected function protectedTest()
	{
		echo 'Protected function two';
		echo "<br>";
	}

	private function privateTest()
	{
		echo 'Private function two';
		echo "<br>";
	}

	public function displayAllFunctions()
	{
		$this->publicTest();
		$this->protectedTest();
		$this->privateTest();
	}
}