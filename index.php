<?php

require 'include/User.php';

$user = new User('Jovan', 'jovan@gmail.com');

$user->userName();

$user->userEmail();

$user2 = new User();

$user2->name = 'Damjan';
$user2->email = 'damjan@gmail.com';
// $user2->age = 37; Results in error because i can not access private property of object
// $user2->height = 170; Results in error because i can not access protected property of object

$user2->userName();

$user2->userEmail();

$user2->userAge();

$user2->userHeight();

$user2->userDetails(); // prints all user details including age and height which are protected and private

//////////////////////////////////////////////////////////////////////////////////////////////////////

require 'include/Test.php';

$test = new Test();

$test->publicTest();

// $test->protectedTest(); Results in error because i can not access protected function

// $test->privateTest(); Results in error because i can not access private function

$test->runAllFunctions(); // runs all functions including protected and private because runAllFunctions method is public

//////////////////////////////////////////////////////////////////////////////////////////////////////

require 'include/Testing.php';

$testing = new Testing();

// $testing->protectedTest(); Results in error because i can not access protected function

// $testing->privateTest(); Results in error because i can not access private function

$testing->displayAllFunctions(); // prints all functions including publicTest method from Test class, protected and private methods can be run via public method/function

$testing->showConstants(); // prints all constants including protected and private, showConstants function is public, constant visibility is added in php 7.1

echo "<br>";
echo "<br>";

echo Testing::PUBLIC_VARIABLE;

echo "<br>";

// echo Testing::PROTECTED_VARIABLE; Results in error because i can not access protected constant

// echo Testing::PRIVATE_VARIABLE; Results in error because i can not access private constant

$testing2 = new Testing();

echo "<br>";
echo "<br>";

//////////////////////////////////////////////////////////////////////////////////////////////////////

require 'database/Data.php';

$data = new Data();

$records = $data->showUsers();

if( $records->num_rows > 0 ){
	while( $person = $records->fetch_object() ){
		echo $person->name;
	}
}

$data2 = new Data();

// $data2->insertUser();

//////////////////////////////////////////////////////////////////////////////////////////////////////

echo "<br>";
echo "<br>";

require 'extra/Dev.php';

$dev = new Dev();

Dev::development();

echo Dev::$dev;

Dev::printResults();

echo "<br>";
echo "<br>";

//////////////////////////////////////////////////////////////////////////////////////////////////////

require 'extra/Extra.php';

$extraObject = new Extra('one', 'two', 'three');

$extraObject2 = new Extra();

echo $extraObject2;

$extraObject3 = new Extra();

$extraObject3->w = 'test';

$extraObject3->showData();

echo "<br>";