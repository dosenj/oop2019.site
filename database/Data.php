<?php

require 'database/DataBase.php';

class Data extends DataBase 
{
	private $host = 'localhost';
	private $username = 'root';
	private $password = 'aurora';
	private $db = 'oop2019';

	private $connection;

	public function __construct()
	{
		$connect = new mysqli($this->host, $this->username, $this->password, $this->db);
		$this->connection = $connect;
	}

	public function showUsers()
	{
		$result = $this->connection->query('SELECT * FROM users');
		return $result;
	}

	public function insertUser()
	{
		$userRecord = $this->connection->query('INSERT INTO users(name, email) VALUES("Foo", "foo@gmail.com")');
		return $userRecord;
	}
}