<?php 

abstract class DataBase
{
	abstract public function showUsers();

	abstract public function insertUser();
}